const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/movie', (request, response) => {
    const connection = db.connect();
    const statement = `select id, title, shortDescription, year, rating, directors, writers, stars, genre, storyline, thumbnail from Movie`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/movie/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `select id, title, shortDescription, year, rating, directors, writers, stars, genre, storyline, thumbnail from Movie where id = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/movie', (request, response) => {
    const {title, shortDescription, year, directors, writers, stars, genre, storyline} = request.body;
    const connection = db.connect();
    const statement = `insert into Movie
            (title, shortDescription, year, directors, writers, stars, genre, storyline) values 
            ('${title}', '${shortDescription}', '${year}', '${directors}', '${writers}', '${stars}', '${genre}', '${storyline}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/movie/:id', (request, response) => {
    const id = request.params.id;
    const {title, shortDescription, year, rating, directors, writers, stars, genre, storyline} = request.body;
    const connection = db.connect();
    const statement = `update Movie
        set
            title = '${title}',
            shortDescription = '${shortDescription}',
            year = '${year}',
            rating = '${rating}',
            directors = '${directors}',
            writers = '${writers}',
            stars = '${stars}',
            genre = '${genre}',
            storyline = '${storyline}'
        where id = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/movie/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `delete from Movie where id = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;