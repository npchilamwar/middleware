const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/feedback', (request, response) => {
    const connection = db.connect();
    const statement = `select review, name, email, comments from Feedback`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/feedback/:name', (request, response) => {
    const name  = request.params.name;
    const connection = db.connect();
    const statement = `select review, name, email, comments from Feedback where name = "${name}"`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.post('/feedback', (request, response) => {
    const {review, name, email, comments} = request.body;
    const connection = db.connect();
    const statement = `insert into Feedback
            (review, name, email, comments) values 
            ('${review}', '${name}', '${email}', '${comments}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/feedback/:name', (request, response) => {
    const id = request.params.name;
    const {review, name, email, comments} = request.body;
    const connection = db.connect();
    const statement = `update Feedback
        set
        review = '${review}',
        name = '${name}', 
        email = '${email}', 
        comments = '${comments}'
        where name = "${id}"`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/feedback/:name', (request, response) => {
    const name = request.params.name;
    const connection = db.connect();
    const statement = `delete from Feedback where name = "${name}"`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;