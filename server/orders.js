const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/orders', (request, response) => {
    const connection = db.connect();
    const statement = `select orderId, custId, orderDate from Orders`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/orders/:orderId', (request, response) => {
    const orderId = request.params.orderId;
    const connection = db.connect();
    const statement = `select orderId, custId, orderDate from Orders where orderId = ${orderId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/orders', (request, response) => {
    const {custId,orderDate} = request.body;
    const connection = db.connect();
    const statement = `insert into Orders
            (custId, orderDate) values 
            ('${custId}','${orderDate}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/orders/:orderId', (request, response) => {
    const custId = request.params.custId;
    const {orderDate} = request.body;
    const connection = db.connect();
    const statement = `update Orders
        set
        orderDate = '${orderDate}'
        where orderId = ${orderId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/orders/:orderId', (request, response) => {
    const orderId = request.params.orderId;
    const connection = db.connect();
    const statement = `delete from Orders where orderId = ${orderId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;