const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/vendor', (request, response) => {
    const connection = db.connect();
    const statement = `select vendorId, vendorName, vendorMobile, vendorAddress from Vendor`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/vendor/:vendorId', (request, response) => {
    const vendorId = request.params.vendorId;
    const connection = db.connect();
    const statement = `select vendorId, vendorName, vendorMobile, vendorAddress from Vendor where vendorId = ${vendorId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/vendor', (request, response) => {
    const {vendorId,vendorName, vendorMobile, vendorAddress} = request.body;
    const connection = db.connect();
    const statement = `insert into Vendor
            (vendorId, vendorName, vendorMobile, vendorAddress) values 
            ('${vendorId}', '${vendorName}', '${vendorMobile}', '${vendorAddress}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/vendor/:vendorId', (request, response) => {
    const vendorId = request.params.vendorId;
    const {vendorName, vendorMobile, vendorAddress} = request.body;
    const connection = db.connect();
    const statement = `update Vendor
        set
        vendorName = '${vendorName}',
        vendorMobile = '${vendorMobile}', 
        vendorAddress = '${vendorAddress}' 
        where vendorId = ${vendorId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/vendor/:vendorId', (request, response) => {
    const vendorId = request.params.vendorId;
    const connection = db.connect();
    const statement = `delete from Vendor where vendorId = ${vendorId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;