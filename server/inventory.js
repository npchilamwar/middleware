const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/inventory', (request, response) => {
    const connection = db.connect();
    const statement = `select productId, productName, vendorId, vendorName, pricePerItem, productStock, productTax, updateDate from Inventory`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/inventory/:productId', (request, response) => {
    const productId = request.params.productId;
    const connection = db.connect();
    const statement = `select productId, productName, vendorId, vendorName, pricePerItem, productStock, productTax, updateDate from Inventory where productId = ${productId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/inventory', (request, response) => {
    const {productName, vendorId, vendorName, pricePerItem, productStock, productTax, updateDate} = request.body;
    const connection = db.connect();
    const statement = `insert into Inventory
            (productName, vendorId, vendorName, pricePerItem, productStock, productTax, updateDate) values 
            ('${productName}', '${vendorId}', '${vendorName}', '${pricePerItem}', '${productStock}', '${productTax}', '${updateDate}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/inventory/:productId', (request, response) => {
    const productId = request.params.productId;
    const {productName, vendorId, vendorName, pricePerItem, productStock, productTax, updateDate} = request.body;
    const connection = db.connect();
    const statement = `update Inventory
        set
        productName = '${productName}',
        vendorId = '${vendorId}', 
        vendorName = '${vendorName}', 
        pricePerItem = '${pricePerItem}', 
        productStock = '${productStock}',
        productTax = '${productTax}',
        updateDate = '${updateDate}'
        where productId = ${productId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/inventory/:productId', (request, response) => {
    const productId = request.params.productId;
    const connection = db.connect();
    const statement = `delete from Inventory where productId = ${productId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;