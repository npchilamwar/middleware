const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/orderdetails', (request, response) => {
    const connection = db.connect();
    const statement = `select orderDetailsId, orderId, productId, productName, quantity, pricePerItem, total from OrderDetails`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/orderdetails/:orderdetailsId', (request, response) => {
    const orderdetailsId = request.params.orderdetailsId;
    const connection = db.connect();
    const statement = `select orderDetailsId, orderId, productId, productName, quantity, pricePerItem, total from OrderDetails where orderdetailsId = ${orderdetailsId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/orderdetails', (request, response) => {
    const {productId, productName, quantity, pricePerItem, total} = request.body;
    const connection = db.connect();
    const statement = `insert into OrderDetails
            (productId, productName, quantity, pricePerItem, total) values 
            ('${productId}', '${productName}', '${quantity}', '${pricePerItem}', '${total}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/orderdetails/:orderdetailsId', (request, response) => {
    const orderdetailsId = request.params.orderdetailsId;
    const {productId, productName, quantity, pricePerItem, total} = request.body;
    const connection = db.connect();
    const statement = `update OrderDetails
        set
        productId = '${productId}',
        productName = '${productName}', 
        quantity = '${quantity}', 
        pricePerItem = '${pricePerItem}', 
        total = '${total}'
        where orderdetailsId = ${orderdetailsId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/orderdetails/:orderdetailsId', (request, response) => {
    const orderdetailsId = request.params.orderdetailsId;
    const connection = db.connect();
    const statement = `delete from OrderDetails where orderdetailsId = ${orderdetailsId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;