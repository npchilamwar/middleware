const express = require('express');
const bodyParser = require('body-parser');

const movieRouter = require('./movie');
const userRouter = require('./user');
const employeeRouter = require('./employee')
const customerRouter = require('./customer')
const feedbackRouter = require('./feedback')
const inventoryRouter = require('./inventory')
const orderdetailsRouter = require('./orderdetails')
const orderRouter = require('./orders')
const purchaseorderRouter = require('./purchaseorder')
const vendorRouter = require('./vendor')



const app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

// routers
app.use(movieRouter);
app.use(userRouter);
app.use(employeeRouter);
app.use(customerRouter);
app.use(feedbackRouter);
app.use(inventoryRouter);
app.use(orderdetailsRouter);
app.use(orderRouter);
app.use(purchaseorderRouter);
app.use(vendorRouter)

app.listen(3000, '0.0.0.0', () => {
    console.log(`Server started on 3000`);
});