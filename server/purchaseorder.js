const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/purchaseOrder', (request, response) => {
    const connection = db.connect();
    const statement = `select vendorId, productId, productName, purchasePricePerItem, quantity, total, purchaseDate from PurchaseOrder`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/purchaseOrder/:vendorId', (request, response) => {
    const vendorId = request.params.vendorId;
    const connection = db.connect();
    const statement = `select vendorId, productId, productName, purchasePricePerItem, quantity, total, purchaseDate from PurchaseOrder where vendorId = ${vendorId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/purchaseOrder', (request, response) => {
    const {productId, productName, purchasePricePerItem, quantity, total, purchaseDate} = request.body;
    const connection = db.connect();
    const statement = `insert into PurchaseOrder
            (productId, productName, purchasePricePerItem, quantity, total, purchaseDate) values 
            ('${productId}', '${productName}', '${purchasePricePerItem}', '${quantity}', '${total}', '${purchaseDate}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/purchaseOrder/:vendorId', (request, response) => {
    const vendorId = request.params.vendorId;
    const {productId, productName, purchasePricePerItem, quantity, total, purchaseDate} = request.body;
    const connection = db.connect();
    const statement = `update PurchaseOrder
        set
        productId= '${productId}',
        productName = '${productName}',
        purchasePricePerItem = '${purchasePricePerItem}',
        quantity = '${quantity}',
        total = '${total}',
        prchaseDate = '${purchaseDate}'
        where vendorId = ${vendorId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/purchaseOrder/:vendorId', (request, response) => {
    const orderId = request.params.orderId;
    const connection = db.connect();
    const statement = `delete from PurchaseOrder where vendorId = ${vendorId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;