const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/customer', (request, response) => {
    const connection = db.connect();
    const statement = `select custId, custFirstName, custLastName, custMobile, custEmail, custPan from Customer`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/customer/:custId', (request, response) => {
    const custId = request.params.custId;
    const connection = db.connect();
    const statement = `select custId, custFirstName, custLastName, custMobile, custEmail, custPan from Customer where custId = ${custId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/customer', (request, response) => {
    const {custFirstName, custLastName, custMobile, custEmail, custPan} = request.body;
    const connection = db.connect();
    const statement = `insert into Customer
            (custFirstName, custLastName, custMobile, custEmail, custPan) values 
            ('${custFirstName}', '${custLastName}', '${custMobile}', '${custEmail}', '${custPan}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/customer/:custId', (request, response) => {
    const custId = request.params.custId;
    const {custFirstName, custLastName, custMobile, custEmail, custPan} = request.body;
    const connection = db.connect();
    const statement = `update Customer
        set
        custFirstName = '${custFirstName}',
        custLastName = '${custLastName}', 
        custMobile = '${custMobile}', 
        custEmail = '${custEmail}', 
        custPan = '${custPan}'
        where custId = ${custId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/customer/:custId', (request, response) => {
    const custId = request.params.custId;
    const connection = db.connect();
    const statement = `delete from Customer where custId = ${custId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;