const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();

router.get('/employee', (request, response) => {
    const connection = db.connect();
    const statement = `select employeeId, password, empFirstName, empLastName, role, address, empUID, dateOfJoining, mobileNo, emailId from Employee`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/employee/:employeeId', (request, response) => {
    const employeeId = request.params.employeeId;
    const connection = db.connect();
    const statement = `select employeeId, password, empFirstName, empLastName, role, address, empUID, dateOfJoining, mobileNo, emailId from Employee where employeeId = ${employeeId}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/employee', (request, response) => {
    const {password, empFirstName, empLastName, role, address, empUID, dateOfJoining, mobileNo, emailId} = request.body;
    const connection = db.connect();
    const statement = `insert into Employee
            (password, empFirstName, empLastName, role, address, empUID, dateOfJoining, mobileNo, emailId) values 
            ('${password}', '${empFirstName}', '${empLastName}', '${role}', '${address}', '${empUID}', '${dateOfJoining}', '${mobileNo}', '${emailId}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/employee/:employeeId', (request, response) => {
    const id = request.params.employeeId;
    const {empFirstName, empLastName, role, address, empUID, dateOfJoining, mobileNo, emailId} = request.body;
    const connection = db.connect();
    const statement = `update Employee
        set
            empFirstName = '${empFirstName}',
            empLastName = '${empLastName}', 
            role = '${role}', 
            address = '${address}', 
            empUID = '${empUID}', 
            dateOfJoining = '${dateOfJoining}', 
            mobileNo = '${mobileNo}', 
            emailId = '${emailId}'
        where employeeId = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/employee/:employeeId', (request, response) => {
    const id = request.params.employeeId;
    const connection = db.connect();
    const statement = `delete from Employee where employeeId = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

module.exports = router;